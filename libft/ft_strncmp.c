/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 01:40:50 by pmatle            #+#    #+#             */
/*   Updated: 2017/07/25 13:25:32 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	if (n == 0)
		return (0);
	while (n - 1 > 0 && *s1 == *s2 && *s1 != '\0' && s2 != '\0')
	{
		s1++;
		s2++;
		n--;
	}
	return ((const unsigned char)*s1 - (const unsigned char)*s2);
}
