/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/14 11:12:14 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/04 11:54:40 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "libft/libft.h"

# define BUFF_SIZE 750

typedef struct	s_static
{
	char	buff[BUFF_SIZE + 1];
	char	*save;
	char	*sub;
	int		x;
	int		ret;
}				t_static;

int				get_next_line(const int fd, char **line);

#endif
