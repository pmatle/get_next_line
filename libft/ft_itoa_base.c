/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 15:19:48 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/08 08:18:02 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_convert_base(long long n, int base, char *s, int *index)
{
	char	*str;

	str = (char *)"0123456789ABCDEF";
	if (n < base)
	{
		s[*index] = str[n];
		*index += 1;
	}
	else
	{
		ft_convert_base(n / base, base, s, index);
		ft_convert_base(n % base, base, s, index);
	}
}

static void		ft_convert_base1(long long n, int base, char *s, int *index)
{
	char	*str;

	str = (char*)"0123456789abcdef";
	if (n < base)
	{
		s[*index] = str[n];
		*index += 1;
	}
	else
	{
		ft_convert_base1(n / base, base, s, index);
		ft_convert_base1(n % base, base, s, index);
	}
}

static int		ft_intlen_base(int n, int base)
{
	int		x;

	x = 0;
	if (n < 0 && base == 10)
	{
		x++;
		n = n * -1;
	}
	while (n)
	{
		x++;
		n = n / base;
	}
	return (x);
}

char			*ft_itoa_base(long long n, int base, char c)
{
	char	*s;
	int		len;
	int		sign;

	len = ft_intlen_base(n, base);
	sign = n < 0 ? 1 : 0;
	n = sign == 1 ? -n : n;
	s = ft_strnew(len + 1);
	if (s == NULL)
		return (NULL);
	sign ? s[0] = '-' : 0;
	if (c == 's')
		ft_convert_base1(n, base, s, &sign);
	else	
		ft_convert_base(n, base, s, &sign);
	return (s);
}
