/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 01:18:20 by pmatle            #+#    #+#             */
/*   Updated: 2017/09/29 13:55:58 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_atoi(const char *str)
{
	int	x;
	int	sign;
	int	answer;

	x = 0;
	sign = 1;
	answer = 0;
	if (ft_strlen(str) == 0)
		return (0);
	while (str[x] == ' ' || str[x] == '\t' || str[x] == '\v' || str[x] == '\f'
			|| str[x] == '\r' || str[x] == '\n')
		x++;
	if (str[x] == '-')
	{
		sign = -1;
		x++;
	}
	else if (str[x] == '+')
		x++;
	while (str[x] != '\0' && str[x] >= '0' && str[x] <= '9')
	{
		answer = answer * 10 + (str[x] - '0');
		x++;
	}
	return (answer * sign);
}
