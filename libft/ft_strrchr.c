/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/24 01:35:43 by pmatle            #+#    #+#             */
/*   Updated: 2017/06/11 17:11:59 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*str;
	int		x;

	x = ft_strlen(s);
	str = (char*)(s + x);
	while (x >= 0)
	{
		if (*str != (char)c)
			str--;
		else
			return (str);
		x--;
	}
	return (NULL);
}
