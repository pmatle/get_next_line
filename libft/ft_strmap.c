/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 02:51:34 by pmatle            #+#    #+#             */
/*   Updated: 2017/07/31 10:50:11 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	int		x;
	int		y;

	if (s == NULL)
		return (NULL);
	x = ft_strlen((char*)s);
	y = 0;
	str = (char*)malloc(sizeof(*str) * x + 1);
	if (!str)
		return (NULL);
	while (s[y] != '\0')
	{
		str[y] = (*f)(s[y]);
		y++;
	}
	str[y] = '\0';
	return (str);
}
